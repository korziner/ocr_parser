import streamlit as st

import cv2
import numpy as np

from utils import draw_rectangles_on_image, display_images_and_results, perform_ocr, save_results_to_file

st.title("Волостной сигнализатор")

uploaded_file = st.file_uploader("Выберите изображение...", type="jp2")

if uploaded_file is not None:
    image = cv2.imdecode(np.frombuffer(uploaded_file.read(), dtype=np.uint8), 1)
    processed_image = image.copy()

    result = perform_ocr(image)
    img = draw_rectangles_on_image(processed_image, result)
    display_images_and_results(image, img, result)
    save_results_to_file(result, uploaded_file)