import os
import cv2 as cv
from fastapi import FastAPI, File, UploadFile
from fastapi.responses import JSONResponse
import uvicorn

app = FastAPI()


@app.post("/one_shot_predict")
async def create_upload_file(object_file: UploadFile = File(...)):
    pass


@app.post("/video_predict")
async def video_predict(video_file: UploadFile = File(...)):
    pass


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
