import easyocr
import cv2

path = None  # path to img
if path is not None:
    reader = easyocr.Reader(['ru', 'en'])
    result = reader.readtext(path, blocklist=';:.,()', width_ths=0.7, decoder='beamsearch')

    # print(result)

    img = cv2.imread(path)

    for (coord, text, prob) in result:
        print(text)
        (topleft, topright, bottomright, bottomleft) = coord
        tx, ty = (int(topleft[0]), int(topleft[1]))
        bx, by = (int(bottomright[0]), int(bottomright[1]))
        cv2.rectangle(img, (tx, ty), (bx, by), (255, 0, 0), 2)

    cv2.imwrite("./img.png", img)
    # cv2.imshow()
    # cv2.waitKey(0)
    # cv2.destroyWindow()
